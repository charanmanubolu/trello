//Create a function getBoard which takes the boardId as argument and returns a promise which resolves with board data

const { apiKey, token, boardsid } = require("./apikey")


function getBoards(id) {
return new Promise((resolve, reject) => {
    
    fetch(`https://api.trello.com/1/boards/${id}?key=${apiKey}&token=${token}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(response => {
            console.log(
                `Response: ${response.status} ${response.statusText}`
            );
            return response.json();
        })
        .then(text => resolve(text)) 
        .catch(err => reject(err));
})
}





getBoards(boardsid)
    .then(data=>{
        console.log(data)
    })
    .catch(err=>console.log(err))

// curl https://api.trello.com/1/members/me/boards?fields=name,url&key=6b31bbf8e374294e6839a63f9253af91&token=ATTA411103b0d29ae02690a314ad0d9a3027416027d57285ef7c1af1eb7dc664d32bB21F686D
