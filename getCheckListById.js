//const {apikey , boardsid} = require('./apikey')
const Token = "ATTA411103b0d29ae02690a314ad0d9a3027416027d57285ef7c1af1eb7dc664d32bB21F686D"
let apiKey= "6b31bbf8e374294e6839a63f9253af91"

function getItems(id) {
    return new Promise((resolve, reject) => {
      fetch(
        `https://api.trello.com/1/checklists/${id}/checkItems?key=${apiKey}&token=${Token}`,
        {
          method: "GET",
        }
      )
        .then((response) => {
          console.log(`Response: ${response.status} ${response.statusText}`);
          return response.json();
        })
        .then((text) => resolve(text))
        .catch((err) => reject(err));
    });
  }
  
function getCheckLists(cardId) {
 
    return new Promise((resolve, reject) => {
      fetch(
        `https://api.trello.com/1/cards/${cardId}/checklists?key=${apiKey}&token=${Token}`,
        {
          method: "GET",
        }
      )
        .then((response) => {
          console.log(`Response: ${response.status} ${response.statusText}`);
          return response.json();
        })
        .then((responseData) => {
          console.log(responseData,"resp")
          resolve(responseData)
        })
        .catch((error) => {
          reject(error);
        });
    });
  }
  
  


function updateCheckItem(cardId, checkItemId) {
  return new Promise((resolve, reject) => {
    fetch(
      `https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?key=${apiKey}&token=${Token}&state=true`,
      {
        method: "PUT",
      }
    )
      .then((response) => {
        console.log(`Response: ${response.status} ${response.statusText}`);
        return response.json();
      })
      .then((text) => resolve(text))
      .catch((err) => reject(err));
  });
}


module.exports = { getCheckLists, getItems, updateCheckItem };
