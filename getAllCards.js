//Create a function getAllCards which takes a boardId as argument and 
//which uses getCards function to fetch cards of all the lists. 
//Do note that the cards should be fetched simultaneously from all the lists.

const {boardsid} = require('./apikey')
const getLists = require('./getList')
const getCards = require("./getCards")

function getAllCards(id) {
    return new Promise((resolve, reject) => {
        
        getLists(id)
            .then((data)=>{
                let cards = []
                for (const list of data) {

                    promise= getCards(list.id)
                    cards.push(promise)
                }
                return Promise.all(cards)
            })
            .then(result=>{
                resolve(result)
            })
            .catch(err=>console.log(err))
    })
    
}


getAllCards(boardsid)
    .then(data=>{
        console.log(data)
    })