//Create a new board, create 3 lists simultaneously, and a card in each list simultaneously

const createBoard = require("./createBoards")
const createList = require("./createList")
const createCard = require("./createCard")


function createNewBoard() {

  return new Promise((resolve, reject) => {
    createBoard("demo2")
      .then((board) => {
        return board.id;
      })
      .then((boardId) => {
        let list = [];
        for (let index = 0; index < 3; index++) {
          list.push(createList(`list${index}`, boardId));
        }
        return Promise.all(list);
      })
      .then((list) => {
        let creatcard = [];
        let listIds = [];
        for (list of list) {
          creatcard.push(createCard(JSON.parse(list).id));
          listIds.push(JSON.parse(list).id);
        }
        resolve(listIds);
        return Promise.all(creatcard);
      })
      .catch((err) => {
        reject(err);
      });
  });

}


module.exports = createNewBoard