//creating  cards

const { token, apiKey } = require("./apikey");

function createCard(id) {
    return new Promise((resolve, reject) => {

        fetch(`https://api.trello.com/1/cards?idList=${id}&key=${apiKey}&token=${token}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json'
            }
        })
            .then(response => {
                console.log(
                    `Response: ${response.status} ${response.statusText}`
                );
                return response.json();
            })
            .then(text => resolve(text))
            .catch(err => reject(err));
    })
}

module.exports=createCard