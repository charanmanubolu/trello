//Create a function getCards which takes a listId as argument and returns a promise which resolves with cards data


//const { apiKey, token, boardsid } = require("./apikey")
let apiKey = "6b31bbf8e374294e6839a63f9253af91"
let token = "ATTA411103b0d29ae02690a314ad0d9a3027416027d57285ef7c1af1eb7dc664d32bB21F686D"

function getCards(id) {
    return new Promise((resolve, reject) => {
        fetch(`https://api.trello.com/1/lists/${id}/cards?key=${apiKey}&token=${token}`, {
            method: 'GET'
        })

        .then(response => {
            console.log(
                `Response: ${response.status} ${response.statusText}`
            );
            return response.json();
        })
        .then(text => resolve(text))
        .catch(err => reject(err));
})
}

module.exports=getCards
// getCards("66541ed6b128234ee5516a6a").then((data) => {
//     console.log(data)
// })