//Create a function createBoard which takes the boardName as argument and returns a promise which resolves with newly created board data

const {apiKey,token,boardsid,name} = require("./apikey")

function createBoard(name) { 
return new Promise((resolve, reject) => {
  
  fetch(`https://api.trello.com/1/boards/?name=${name}&key=${apiKey}&token=${token}`, {
      method: 'POST'
    })
      .then(response => {
        console.log(
          `Response: ${response.status} ${response.statusText}`
        );
        return response.json();
      })
      .then(text => resolve(text))
      .catch(err => reject(err));
})
}

  module.exports=createBoard