//Create a function getLists which takes a boardId as argument and returns a promise which resolved with lists data

//const { apiKey, token, boardsid } = require("./apikey")
let apiKey= "6b31bbf8e374294e6839a63f9253af91"
let token= "ATTA411103b0d29ae02690a314ad0d9a3027416027d57285ef7c1af1eb7dc664d32bB21F686D"

function getLists(id) {
    return new Promise((resolve, reject) => {

        fetch(`https://api.trello.com/1/boards/${id}/lists?key=${apiKey}&token=${token}`, {
            method: 'GET',
            
        })
            .then(response => response.json())
            .then(data => resolve(data))
            .catch(err => reject(err));
    })
}

module.exports = getLists
