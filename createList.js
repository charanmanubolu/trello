const { token, apiKey } = require("./apikey");

//creating  a list
function createList(list , id) {
    return new Promise((resolve, reject) => {

        fetch(`https://api.trello.com/1/boards/${id}/lists?name=${list}&key=${apiKey}&token=${token}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json'
            }
        })
            .then(response => {
                console.log(
                    `Response: ${response.status} ${response.statusText}`
                );
                return response.text();
            })
            .then(text => resolve(text))
            .catch(err => reject(err));
    })

}

module.exports=createList